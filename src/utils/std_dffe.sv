module std_dffe
#(
    parameter WIDTH = 8
)
(
    input                       clk,
    input                       en,
    input       [WIDTH-1:0]     d,
    output      [WIDTH-1:0]     q
);

logic    [WIDTH-1:0] dff_q;

always_ff @(posedge clk) begin
    if(en) begin
        dff_q <= d;
    end
end
assign  q = dff_q;

endmodule
