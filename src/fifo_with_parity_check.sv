module fifo_with_parity_check 
#(
  parameter int unsigned DEPTH      = 4,   // default data width if the fifo is of type logic
  parameter int unsigned DEPTH_W    = DEPTH > 1 ? $clog2(DEPTH) : 1,
  parameter int unsigned WIDTH      = 32,
  parameter int unsigned DATA_WIDTH = WIDTH+1,
  
  parameter PARITY     = 0, // 0: EVEN, 1: ODD
  parameter PARITY_BIT = 0  // 0: MSB, 1: LSB
)
(
  input  logic [DATA_WIDTH-1:0] data_i,
  input  logic                  valid_i,
  output logic                  grant_o,

  output logic [DATA_WIDTH-1:0] data_o,
  output logic                  valid_o,
  input  logic                  grant_i,

  input  logic                  clk_i,
  input  logic                  rst_ni
);

logic [DATA_WIDTH-1:0] fifo_to_checker_data;
logic                  fifo_to_checker_valid;
logic                  checker_to_fifo_grant;

fifo #(
  .DEPTH      (DEPTH      ),
  .WIDTH      (WIDTH      ),
  .DATA_WIDTH (DATA_WIDTH ),
  .PARITY     (PARITY     ),
  .PARITY_BIT (PARITY_BIT )
)
fifo_inst
(
  .push_data_i   (data_i ),
  .push_valid_i  (valid_i),
  .push_grant_o  (grant_o),

  .pop_data_o    (fifo_to_checker_data),
  .pop_valid_o   (fifo_to_checker_valid),
  .pop_grant_i   (checker_to_fifo_grant),

  .clk_i         (clk_i ),
  .rst_ni        (rst_ni)
);

parity_checker #(
  .WIDTH      (WIDTH      ),
  .DATA_WIDTH (DATA_WIDTH ),
  .PARITY     (PARITY     ),
  .PARITY_BIT (PARITY_BIT )
)
parity_checker_inst
(
  .fifo_data_i      (fifo_to_checker_data ),
  .fifo_valid_i     (fifo_to_checker_valid),
  .fifo_grant_o     (checker_to_fifo_grant),

  .checked_data_o   (data_o ),
  .checked_valid_o  (valid_o),
  .checked_grant_i  (grant_i),

  .clk_i            (clk_i ),
  .rst_ni           (rst_ni)
);

endmodule