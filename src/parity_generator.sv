// module parity_generator 
// #(
//   parameter IN_DATA_WIDTH  = 32,
//   parameter OUT_DATA_WIDTH = IN_DATA_WIDTH + 1,
//   parameter PARITY     = 0, // 0: EVEN, 1: ODD
//   parameter PARITY_BIT = 0  // 0: MSB, 1: LSB
// )
// (
//   input  logic [IN_DATA_WIDTH-1:0]  data_i,
//   output logic [OUT_DATA_WIDTH-1:0] data_o
// );

// logic even_parity_bit;
// logic odd_parity_bit;
// logic parity_bit;

// assign even_parity_bit = ^data_i;
// assign odd_parity_bit = ~even_parity_bit;
// assign parity_bit = PARITY ? odd_parity_bit : even_parity_bit;

// generate
//   if(PARITY_BIT == 0) begin
//     assign data_o = {parity_bit, data_i};
//   end else begin
//     assign data_o = {data_i, parity_bit};
//   end
// endgenerate

// endmodule

function logic [OUT_DATA_WIDTH-1:0] generate_parity(
  input logic [IN_DATA_WIDTH-1:0]  data_i,
  input logic PARITY,
  input logic PARITY_BIT
);
  logic even_parity_bit;
  logic odd_parity_bit;
  logic parity_bit;
  logic [OUT_DATA_WIDTH-1:0] data_o;

  assign even_parity_bit = ^data_i;
  assign odd_parity_bit = ~even_parity_bit;
  assign parity_bit = PARITY ? odd_parity_bit : even_parity_bit;

  if (PARITY_BIT == 0) begin
    data_o = {parity_bit, data_i};
  end else begin
    data_o = {data_i, parity_bit};
  end

  return data_o;
endfunction