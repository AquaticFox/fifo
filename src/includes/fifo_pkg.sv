package fifo_pkg;
  parameter int unsigned DEPTH      = 4;   // default data width if the fifo is of type logic
  parameter int unsigned DEPTH_W    = DEPTH > 1 ? $clog2(DEPTH) : 1;
  parameter int unsigned WIDTH      = 32;
  parameter int unsigned DATA_WIDTH = WIDTH+1; // 1 more bit for parity info
  
  parameter PARITY     = 0; // 0: EVEN, 1: ODD
  parameter PARITY_BIT = 0; // 0: MSB, 1: LSB
endpackage : fifo_pkg
