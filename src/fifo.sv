module fifo #(
  parameter int unsigned DEPTH      = 4,   // default data width if the fifo is of type logic
  parameter int unsigned DEPTH_W    = DEPTH > 1 ? $clog2(DEPTH) : 1,
  parameter int unsigned WIDTH      = 32,
  parameter int unsigned DATA_WIDTH = WIDTH+1, // 1 more bit for parity info
  parameter type dtype              = logic [DATA_WIDTH-1:0],
  
  parameter PARITY     = 0, // 0: EVEN, 1: ODD
  parameter PARITY_BIT = 0  // 0: MSB, 1: LSB
)
(
  input  logic [DATA_WIDTH-1:0] push_data_i,
  input  logic                  push_valid_i,
  output logic                  push_grant_o,

  output logic [DATA_WIDTH-1:0] pop_data_o,
  output logic                  pop_valid_o,
  input  logic                  pop_grant_i,

  input  logic                  clk_i,
  input  logic                  rst_ni
);

logic fifo_empty;
logic fifo_full;

logic push_hsk;
logic pop_hsk;

dtype [DEPTH-1:0] fifo_data_d, fifo_data_q;
logic [DEPTH-1:0] fifo_data_ena;

logic [DEPTH_W-1:0] write_ptr_d, write_ptr_q;
logic               write_ptr_ena;
logic [DEPTH_W-1:0] read_ptr_d,  read_ptr_q;
logic               read_ptr_ena;
logic [DEPTH_W:0]   free_entry_num_d, free_entry_num_q;
logic               free_entry_num_ena;

logic [DATA_WIDTH-1:0] push_data_with_parity;
  
assign fifo_empty = free_entry_num_q == DEPTH;
assign fifo_full  = free_entry_num_q == '0;

assign push_hsk   = push_valid_i & push_grant_o;
assign pop_hsk    = pop_valid_o  & pop_grant_i;


// next states
assign write_ptr_d   = (write_ptr_q == DEPTH-1) ? '0 : write_ptr_q + 1;
assign write_ptr_ena = push_hsk;

assign read_ptr_d    = (read_ptr_q  == DEPTH-1) ? '0 : read_ptr_q  + 1;
assign read_ptr_ena  = pop_hsk;

always_comb begin
  free_entry_num_d   = free_entry_num_q;
  free_entry_num_ena = '0;
  unique case({push_hsk, pop_hsk})
    2'b01: begin // only pop 1 entry
      free_entry_num_d   = free_entry_num_q + 1;
      free_entry_num_ena = 1'b1;
    end
    2'b10: begin // only push 1 entry
      free_entry_num_d   = free_entry_num_q - 1;
      free_entry_num_ena = 1'b1;
    end
    default: begin
      // don't change the number
    end
  endcase
end

// next data
always_comb begin
  fifo_data_d   = fifo_data_q;
  fifo_data_ena = '0;
  if(push_hsk) begin
    fifo_data_d  [write_ptr_q] = push_data_with_parity;
    fifo_data_ena[write_ptr_q] = 1'b1;
  end
end

// output signals
assign push_grant_o = ~fifo_full;
assign pop_valid_o  = ~fifo_empty;
assign pop_data_o   = fifo_data_q[read_ptr_q];

// parity data
// parity_generator 
// #(
//   .IN_DATA_WIDTH  (WIDTH      ),
//   .OUT_DATA_WIDTH (DATA_WIDTH ),
//   .PARITY         (PARITY     ),
//   .PARITY_BIT     (PARITY_BIT )
// )
// parity_generator_inst
// (
//   .data_i   (push_data_i),
//   .data_o   (push_data_with_parity)
// );
assign push_data_with_parity = push_data_i;

// control regs
std_dffre
#(.WIDTH(DEPTH_W))
U_FIFO_WRITE_PTR_REG
(
  .clk      (clk_i        ),
  .rstn     (rst_ni       ),
  .en       (write_ptr_ena),
  .d        (write_ptr_d  ),
  .q        (write_ptr_q  )
);

std_dffre
#(.WIDTH(DEPTH_W))
U_FIFO_READ_PTR_REG
(
  .clk      (clk_i        ),
  .rstn     (rst_ni       ),
  .en       (read_ptr_ena ),
  .d        (read_ptr_d   ),
  .q        (read_ptr_q   )
);

std_dffrve
#(.WIDTH(DEPTH_W+1))
U_FIFO_FREE_ENTRY_NUM_REG
(
  .clk      (clk_i             ),
  .rstn     (rst_ni            ),
  .rst_val  (DEPTH[DEPTH_W:0]  ),
  .en       (free_entry_num_ena),
  .d        (free_entry_num_d  ),
  .q        (free_entry_num_q  )
);

// data payload
generate
  for(genvar i = 0; i < DEPTH; i++) begin: gen_fifo_data
    std_dffe
    #(.WIDTH(DATA_WIDTH))
    U_FIFO_DATA_REG
    (
      .clk      (clk_i            ),
      .en       (fifo_data_ena[i] ),
      .d        (fifo_data_d  [i] ),
      .q        (fifo_data_q  [i] )
    );
  end
endgenerate

// pseudo_dual_ram
// #(
//   .WIDTH    (DATA_WIDTH ),
//   .DEPTH    (DEPTH      )
// )
// U_FIFO_RAM
// (
//   .ra     (read_ptr_q           ),
//   .re     ('1                   ),
//   .rd     (pop_data_o           ),
//   .wa     (write_ptr_q          ),
//   .we     (push_hsk             ),
//   .wd     (push_data_with_parity),
//   .rst_ni (rst_ni               ),
//   .clk_i  (clk_i                )
// );

endmodule
