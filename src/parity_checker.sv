module parity_checker #(
  parameter int unsigned WIDTH      = 32,
  parameter int unsigned DATA_WIDTH = WIDTH+1, // 1 more bit for parity info
  
  parameter PARITY     = 0, // 0: EVEN, 1: ODD
  parameter PARITY_BIT = 0  // 0: MSB, 1: LSB
)
(
  input  logic [DATA_WIDTH-1:0] fifo_data_i,
  input  logic                  fifo_valid_i,
  output logic                  fifo_grant_o,

  output logic [DATA_WIDTH-1:0] checked_data_o,
  output logic                  checked_valid_o,
  input  logic                  checked_grant_i,

  input  logic                  clk_i,
  input  logic                  rst_ni
);

logic even_parity_check_pass;
logic odd_parity_check_pass;
logic parity_check_pass;

assign odd_parity_check_pass  = ^fifo_data_i;
assign even_parity_check_pass = ~odd_parity_check_pass;
assign parity_check_pass      = PARITY ? odd_parity_check_pass : even_parity_check_pass;

// assign checked_data_o  = PARITY_BIT ? fifo_data_i[1+:WIDTH] : fifo_data_i[0+:WIDTH];
assign checked_data_o  = fifo_data_i;
assign checked_valid_o = fifo_valid_i & parity_check_pass;

assign fifo_grant_o    = checked_grant_i | (fifo_valid_i & ~parity_check_pass);

endmodule