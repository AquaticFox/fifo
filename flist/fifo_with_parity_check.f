$PROJ_ROOT/src/includes/fifo_pkg.sv

// $PROJ_ROOT/src/utils/pseudo_dual_ram.sv
$PROJ_ROOT/src/utils/std_dffe.sv
$PROJ_ROOT/src/utils/std_dffre.sv
$PROJ_ROOT/src/utils/std_dffrve.sv

$PROJ_ROOT/src/fifo.sv
$PROJ_ROOT/src/fifo_with_parity_check.sv
$PROJ_ROOT/src/parity_checker.sv
// $PROJ_ROOT/src/parity_generator.sv

$PROJ_ROOT/tb/testbench.sv