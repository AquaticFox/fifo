`timescale 1ns/1ps
module testbench 
  import fifo_pkg::*;
#(
)
();


logic clk, rst_n;

//clock generate
initial begin
  clk = 1'b0;
  forever #0.5 clk = ~clk;
end

//reset generate
initial begin
  rst_n = 1'b0;
  #20;
  rst_n = 1'b1;
end

//wave dump
initial begin
  int dumpon = 0;
  string log;
  string wav;
  $value$plusargs("dumpon=%d",dumpon);
  if ($value$plusargs("sim_log=%s",log)) begin
      $display("!!!!!!!!!!wave_log= %s",log);
  end
  wav = {log,"/wave.fsdb"};
  $display("!!!!!!wave_log= %s",wav);
  if(dumpon > 0) begin
    $fsdbDumpfile(wav);
    // $fsdbAutoSwitchDumpfile(1000,wav,0);
    $fsdbDumpvars(0,testbench);
    $fsdbDumpvars("+struct");
    $fsdbDumpvars("+mda");
    $fsdbDumpvars("+all");
    $fsdbDumpMDA();
    $fsdbDumpon;
  end
end

// generate parity bit
function logic [DATA_WIDTH-1:0] generate_parity(
  input logic [WIDTH-1:0]  data_i,
  input logic PARITY,
  input logic PARITY_BIT
);
  logic even_parity_bit;
  logic odd_parity_bit;
  logic parity_bit;
  logic [DATA_WIDTH-1:0] data_o;

  assign even_parity_bit = ^data_i;
  assign odd_parity_bit = ~even_parity_bit;
  assign parity_bit = PARITY ? odd_parity_bit : even_parity_bit;

  if (PARITY_BIT == 0) begin
    data_o = {parity_bit, data_i};
  end else begin
    data_o = {data_i, parity_bit};
  end

  return data_o;
endfunction

// dut signals
logic [DATA_WIDTH-1:0] data_to_dut;
logic                  valid_to_dut;
logic                  grant_from_dut;

logic [DATA_WIDTH-1:0] data_from_dut;
logic                  valid_from_dut;
logic                  grant_to_dut;


// verify logic
typedef struct packed {
  bit [DATA_WIDTH-1:0]  data;
  bit                   error_injected;
} golden_fifo_entry_t;

logic [WIDTH-1:0]       pay_load_data;
// logic [DATA_WIDTH-1:0]  data_to_dut_before_error_inject;
logic                   error_injected;

golden_fifo_entry_t golden_fifo[$];
golden_fifo_entry_t golden_fifo_front;
int free_entry = DEPTH;

int iter = 1000;
int error_injection = 1;

int passed_counter  = 0;
int dropped_counter = 0;

always_ff @(posedge clk) begin
  // common output, check data correction
  if (valid_from_dut & grant_to_dut) begin
    passed_counter = passed_counter + 1;
    free_entry     = free_entry + 1;
    golden_fifo_front = golden_fifo.pop_front();
    assert (golden_fifo_front.error_injected == '0)
    else begin
      $fatal("\n Error: Fail when check parity checker");
    end

    assert (data_from_dut == golden_fifo_front.data)
    else begin
      $fatal("\n Error: Fail when check equalation, data_from_dut[%x] -- gloden[%x]", data_from_dut,
              golden_fifo_front.data);
    end
  end
  // inside output hsk, but no output hsk, has to be parity check failed
  else if(fifo_with_parity_check_dut.fifo_to_checker_valid & fifo_with_parity_check_dut.checker_to_fifo_grant) begin
    dropped_counter = dropped_counter + 1;
    free_entry      = free_entry + 1;
    golden_fifo_front = golden_fifo.pop_front();
    assert (golden_fifo_front.error_injected)
    else begin
      $fatal("\n Error: Fail at parity check");
    end
  end

  // input hsk, record the data into golden fifo
  if (valid_to_dut & grant_from_dut) begin
    free_entry = free_entry - 1;
    golden_fifo.push_back({data_to_dut, error_injected});
  end
end

initial begin
  grant_to_dut   = '0;
  valid_to_dut   = '0;
  error_injected = '0;

  #21
  // 1. Full the FIFO
  grant_to_dut = '0;
  valid_to_dut = '0;
  for (int i = 0; i < DEPTH+1; i++) begin
    valid_to_dut  = 1'b1;
    pay_load_data = $urandom();

    // random error injection
    data_to_dut = generate_parity(pay_load_data, PARITY, PARITY_BIT);
    if(error_injection) begin
      if ($urandom_range(0, 10) == 10) begin
        data_to_dut[$urandom_range(0, DATA_WIDTH-1)]^= 1'b1;
        error_injected = 1'b1;
      end
    end

    assert (free_entry == fifo_with_parity_check_dut.fifo_inst.free_entry_num_q)
    else begin
      $fatal("\n Error : free_entry_num_q is not equal, which should never happen! dut[%d] gloden[%d] \n",
              fifo_with_parity_check_dut.fifo_inst.free_entry_num_q, free_entry);
    end

    #1;
    error_injected = '0;
  end
  valid_to_dut = '0;
  $info("\n PASS full_the_fifo_test\n");

  #20
  // 2. Empty the FIFO
  grant_to_dut = '0;
  valid_to_dut = '0;
  for (int i = 0; i < DEPTH+1; i++) begin
    grant_to_dut = 1'b1;

    assert (free_entry == fifo_with_parity_check_dut.fifo_inst.free_entry_num_q)
    else begin
      $fatal("\n Error : free_entry_num_q is not equal, which should never happen! dut[%d] gloden[%d] \n",
              fifo_with_parity_check_dut.fifo_inst.free_entry_num_q, free_entry);
    end

    #1;
  end
  grant_to_dut = '0;
  $info("\n PASS empty_the_fifo_test\n");

  #20
  // 3. Random traffic, at max BW (this means that pop_grant_i is always 1)
  grant_to_dut = '0;
  valid_to_dut = '0;
  
  for (int i = 0; i < iter; i++) begin
    grant_to_dut = '1;
    if ($urandom_range(0, 1)) begin
      valid_to_dut  = 1'b1;
      pay_load_data = $urandom();

      // random error injection
      data_to_dut = generate_parity(pay_load_data, PARITY, PARITY_BIT);
      if(error_injection) begin
        if ($urandom_range(0, 10) == 10) begin
          data_to_dut[$urandom_range(0, DATA_WIDTH-1)]^= 1'b1;
          error_injected = 1'b1;
        end
      end

    end else begin
      valid_to_dut = 1'b0;
    end

    assert (free_entry == fifo_with_parity_check_dut.fifo_inst.free_entry_num_q)
    else begin
      $fatal("\n Error : free_entry_num_q is not equal, which should never happen! dut[%d] gloden[%d] \n",
              fifo_with_parity_check_dut.fifo_inst.free_entry_num_q, free_entry);
    end

    #1;
    error_injected = '0;
  end
  grant_to_dut = '0;
  valid_to_dut = '0;
  $info("\n PASS random_max_bw_test after %d iter \n", iter);

  #20
  // 4. Random traffic, with random pop_grant_i (50% low, 50% high)
  grant_to_dut = '0;
  valid_to_dut = '0;

  for (int i = 0; i < iter; i++) begin
    if ($urandom_range(0, 1)) begin
      valid_to_dut  = 1'b1;
      pay_load_data = $urandom();

      // random error injection
      data_to_dut = generate_parity(pay_load_data, PARITY, PARITY_BIT);
      if(error_injection) begin
        if ($urandom_range(0, 10) == 10) begin
          data_to_dut[$urandom_range(0, DATA_WIDTH-1)]^= 1'b1;
          error_injected = 1'b1;
        end
      end

    end else begin
      valid_to_dut = 1'b0;
    end

    if ($urandom_range(0, 1)) begin
      grant_to_dut = 1'b1;
    end else begin
      grant_to_dut = 1'b0;
    end

    assert (free_entry == fifo_with_parity_check_dut.fifo_inst.free_entry_num_q)
    else begin
      $fatal("\n Error : free_entry_num_q is not equal, which should never happen! dut[%d] gloden[%d] \n",
              fifo_with_parity_check_dut.fifo_inst.free_entry_num_q, free_entry);
    end

    #1;
    error_injected = '0;
  end
  grant_to_dut = 1'b0;
  valid_to_dut = '0;

  $info("\n PASS random_random_bw_test after %d iter \n", iter);

  #20
  $finish;
end

final begin
  $display("passed_counter = %d; dropped_counter = %d;", passed_counter, dropped_counter);
end

// parity generator
// parity_generator 
// #(
//   .IN_DATA_WIDTH  (WIDTH      ),
//   .OUT_DATA_WIDTH (DATA_WIDTH ),
//   .PARITY         (PARITY     ),
//   .PARITY_BIT     (PARITY_BIT )
// )
// parity_generator_inst
// (
//   .data_i   (pay_load_data),
//   .data_o   (data_to_dut_before_error_inject)
// );


// dut
fifo_with_parity_check 
#(
  .DEPTH      (DEPTH      ),
  .WIDTH      (WIDTH      ),
  .DATA_WIDTH (DATA_WIDTH ),
  .PARITY     (PARITY     ), // 0: EVEN, 1: ODD
  .PARITY_BIT (PARITY_BIT )  // 0: MSB, 1: LSB
)
fifo_with_parity_check_dut
(
  .data_i   (data_to_dut    ),
  .valid_i  (valid_to_dut   ),
  .grant_o  (grant_from_dut ),

  .data_o   (data_from_dut  ),
  .valid_o  (valid_from_dut ),
  .grant_i  (grant_to_dut   ),

  .clk_i    (clk    ),
  .rst_ni   (rst_n  )
);
endmodule